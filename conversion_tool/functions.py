import numpy as np
import uuid
import logging

logger = logging.getLogger('conversion_tool.common')


def convert_annotations(annotation):
    annotations = {}
    if annotation['algoType'] == "YOLOv5":
        regions = yolo5_case(annotation)
    elif annotation['algoType'] == "MaskRCNN":
        regions = maskrcnn_case(annotation)
    elif annotation['algoType'] == "ICCS":
        # print("I am in ICCS algoType")
        regions = iccs_case(annotation)
    elif annotation['algoType'] == "USE":
        regions = use_case(annotation)
    else:
        regions = []
        logger.info("Model not recognized")
    annotations['regions'] = regions
    annotations['algoType'] = annotation['algoType']
    return annotations


def yolo5_case(annotation):
    regions = []
    for result in annotation['regions']:
        xyxy = [result["xmin"], result["ymin"], result["xmax"], result["ymax"]]
        coords = xyxy2xywh(np.array(xyxy).reshape(1, -1)).astype(int).tolist()[0]
        region = {
            "id": str(uuid.uuid1())[0:8],
            "type": "polygon",
            "confidence": result["confidence"],
            "tags": [
                "bounding box",
                "yolo5",
                result["name"]
            ],
            "boundingBox": {
                "height": coords[3],
                "width": coords[2],
                "left": coords[0],
                "top": coords[1]
            },
        }
        regions.append(region)
    logger.info("Conversion of YOLOv5 model Completed")
    return regions


def maskrcnn_case(annotation):
    regions = []
    for result in annotation['regions']:
        region = {
            "id": str(uuid.uuid1())[0:8],
            "type": "polygon",
            "confidence": result["confidence"],
            "tags": [
                "bounding box",
                "mascRCNN",
                "corrosion"
            ],
            "boundingBox": {
                "height": result['bbox'][3],
                "width": result['bbox'][2],
                "left": result['bbox'][0],
                "top": result['bbox'][1]
            },
            "points": result["points"]
        }
        regions.append(region)
    logger.info("Conversion of Mask R-CNN model Completed")
    return regions


def iccs_case(annotation):
    regions = []
    for result in annotation['regions']:
        region = {
            "id": result["id"],
            "type": result["type"],
            "confidence": result["confidence"],
            "tags": result["tags"],
            "boundingBox": result["boundingBox"],
            "points": result["points"]
        }
        if "Crack_Points" not in result["tags"]:
            region["metrics"] = result["metrics"]
        regions.append(region)
    logger.info("Conversion of ICCS's output Completed")
    return regions


def use_case(annotation):
    regions = []
    for result in annotation['regions']:
        region = {
            "id": result["id"],
            "type": result["type"],
            "confidence": result["confidence"],
            "tags": result["tags"],
            "boundingBox": {
                "height": result['boundingBox']["height"],
                "width": result['boundingBox']["width"],
                "left": result['boundingBox']["left"],
                "top": result['boundingBox']["top"]
            },
            "points": result["points"]
        }
        regions.append(region)
    # old_format
    # regions = []
    # for category, defect in annotation['regions'][0].items():
    #     if bool(defect):
    #         for key, value in defect.items():
    #             # get coordinates
    #             x1, y1, x2, y2 = value["bbox"]
    #             # calculate width and height of the box
    #             width, height = x2 - x1, y2 - y1
    #             region = {
    #                 "id": str(uuid.uuid1())[0:8],
    #                 "type": "polygon",
    #                 "confidence": value["score"],
    #                 "tags": [
    #                     "bounding box",
    #                     "USE_case",
    #                     category
    #                 ],
    #                 "boundingBox": {
    #                     "height": height,
    #                     "width": width,
    #                     "left": x1,
    #                     "top": y1
    #                 },
    #             }
    #             regions.append(region)
    logger.info("Conversion of USE's output Completed")
    return regions


def xyxy2xywh(x: np.ndarray):
    # Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    # Taken from Yolov5 repo
    y = np.copy(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2  # x center
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2  # y center
    y[:, 2] = x[:, 2] - x[:, 0]  # width
    y[:, 3] = x[:, 3] - x[:, 1]  # height
    return y
