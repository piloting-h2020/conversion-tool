"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

from ast import literal_eval

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from pathlib import Path

here = Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')


def get_version(basepath):
    """Retrieve  __version__ from  __init__ file"""

    finit = basepath / 'conversion_tool' / '__init__.py'
    with open(finit, 'r') as fd:
        for line in fd:
            if line.startswith('__version__'):
                return literal_eval(line.split('=', 1)[1].strip())

    return '0.0.0'


setup(
    name='conversion_tool',
    version=get_version(here),
    description='A harmonization tool for the outputs of different cases',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/inlecom/piloting/conversion-tool',
    classifiers=[
        'Intended Audience :: Developers',
    ],
    keywords='logging, development',
    packages=find_packages(),
    python_requires='>=3.6, <4',
    install_requires=[
        'numpy>=1.20.3'
    ],
    project_urls={
        'Source': 'TODO',
        'Issues': 'TODO'
    },
)
